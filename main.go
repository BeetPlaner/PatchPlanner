package main

import (
	"log"
	"net/http"

	"github.com/gorilla/sessions"
)

var store = sessions.NewCookieStore([]byte("sometsdfg54345AE%&SR&(%§A$&IUj56sj34ret"))

func main() {

	DBInit()

	router := NewRouter()
	router.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))))

	log.Fatal(http.ListenAndServe(":8080", router))
	//log.Fatal(http.ListenAndServeTLS(":8080", "cert.pem", "key.pem", router))

}
