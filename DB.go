package main

import (
	"fmt"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"

	"golang.org/x/crypto/bcrypt"
)

var DB *sqlx.DB
var LastPwd = ""

const dbTIMEFORMAT string = "2006-01-02 15:04:05" //'2007-01-01 10:00:00'

type GenericResult struct {
	Message string
	Code    int
}

func DBInit() {

	var err error
	DB, err = sqlx.Connect("sqlite3", "beet.db")
	if err != nil {
		log.Println(err)
	}

	//	DB.MustExec(schema)

}

var schema = "CREATE TABLE Users ( `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `Email` TEXT NOT NULL UNIQUE, `Password` TEXT NOT NULL, `UserVerified` INTEGER NOT NULL DEFAULT 0, `Created` DATETIME, `LastLogin` DATETIME )"

func dbUserLogin(MailAddress string, clearPassword string) (GenericResult, int) {

	var u User
	u.ID = -1

	err := DB.Get(&u, "SELECT * FROM Users WHERE Email=$1", MailAddress)
	if err != nil {
		log.Println(err)
		return GenericResult{
			Code:    -1,
			Message: "Error logging in",
		}, 0
	}

	loginValid := CheckPasswordHash(clearPassword, u.Password)

	if !loginValid {
		return GenericResult{
			Code:    -3,
			Message: "Invalid Login!",
		}, 0
	}

	loginToken, _ := HashPassword((time.Now().String() + u.Email))
	q := `UPDATE Users SET LastLogin = ?, LoginToken = ? WHERE Email = ?`
	res := DB.MustExec(q, time.Now(), loginToken, u.Email)
	log.Println(res)
	return GenericResult{
		Code:    0,
		Message: loginToken,
	}, u.ID

}

func dbGetLoginToken(Email string) string {
	res := ""

	err := DB.Get(&res, "SELECT LoginToken FROM Users WHERE Email=$1", Email)
	if err != nil {
		return ""
	}

	return res
}

func dbCreateGarden(Name string, UserID int) GenericResult {

	var cnt int
	err := DB.Get(&cnt, "SELECT count(*) as cnt FROM Garden WHERE Owner=? AND Name=?", UserID, Name)
	if cnt > 0 {
		return GenericResult{
			Code:    -1,
			Message: "Garden already exists!",
		}
	}

	g := Garden{
		OwnerID: UserID,
		Name:    Name,
	}

	_, err = DB.NamedExec("INSERT INTO Garden (Name, Owner) VALUES (:Name, :Owner)", g)
	if err != nil {
		return GenericResult{
			Code:    -2,
			Message: "Error Creating Garden!",
		}
	}

	return GenericResult{
		Code:    0,
		Message: "OK",
	}

}

func dbGetGarden(userID int, gardenID int) (Garden, error) {

	// this will pull places with telcode > 50 into the slice pp
	garden := Garden{}
	err := DB.Get(&garden, "SELECT * FROM Garden WHERE Owner = ? AND ID = ?", userID, gardenID)
	if err != nil {
		log.Println(err)
		return garden, err
	}

	return garden, nil
}

func dbGetGardenDetails(userID int, gardenID int) (Garden, error) {

	// this will pull places with telcode > 50 into the slice pp
	garden, _ := dbGetGarden(userID, gardenID)

	patches := []Patch{}
	err := DB.Select(&patches, "Select p.* from Patch as p"+
		" JOIN Garden AS g ON p.GardenID = g.ID"+
		" WHERE p.GardenID = ? and g.Owner = ?", gardenID, userID)

	if err != nil {
		log.Println(err)
		return garden, err
	}

	garden.Patches = patches

	return garden, nil
}

func dbGetGardens(userID int) (error, []Garden) {

	// this will pull places with telcode > 50 into the slice pp
	gardens := []Garden{}
	err := DB.Select(&gardens, "SELECT * FROM Garden WHERE Owner = ?", userID)
	if err != nil {
		log.Println(err)
		return err, nil
	}

	return nil, gardens
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	log.Println("Hashing:" + password + " to " + string(bytes))
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func dbUserCreate(MailAddress string, clearPassword string) GenericResult {

	if dbMailaddressExists(MailAddress) {
		log.Println("Mail already Exists")
		return GenericResult{
			Code:    -2,
			Message: "Mail already Exists",
		}
	}

	hash, err := HashPassword(clearPassword)
	LastPwd = string(hash)
	if err != nil {
		log.Println(err)
	}

	u := User{
		Email:     MailAddress,
		Password:  hash,
		Created:   time.Now(),
		LastLogin: time.Now(),
	}

	DB.NamedExec("INSERT INTO Users (Email, Password, Created, LastLogin) VALUES (:Email, :Password, :Created, :LastLogin)", u)

	return GenericResult{
		Code:    0,
		Message: "OK",
	}
}

func dbMailaddressExists(Email string) bool {

	//Check if name existst
	var cnt int
	err := DB.Get(&cnt, "SELECT count(*) as cnt FROM Users WHERE Email=$1", Email)
	if err != nil {
		fmt.Println(err)
	}

	if cnt > 0 {
		return true
	}

	return false
}
