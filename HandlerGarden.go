package main

import (
	"html/template"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func GardenDetail(w http.ResponseWriter, r *http.Request) {
	if !ValidateAccess(&w, r) {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	vars := mux.Vars(r)

	sess, _ := store.Get(r, "session-name")
	sessUserID, _ := sess.Values["UserID"].(int)
	gardenID, _ := strconv.Atoi(vars["gardenID"])
	userGarden, _ := dbGetGardenDetails(sessUserID, gardenID)

	t, _ := template.ParseFiles("tpl/GardenDetail.html", "tpl/menu.html", "tpl/header.html", "tpl/footer.html")
	t.Execute(w, userGarden)
}

func GardenHome(w http.ResponseWriter, r *http.Request) {
	if !ValidateAccess(&w, r) {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	sess, _ := store.Get(r, "session-name")
	sessUserID, _ := sess.Values["UserID"].(int)
	_, userGarden := dbGetGardens(sessUserID)

	t, _ := template.ParseFiles("tpl/GardenHome.html", "tpl/menu.html", "tpl/header.html", "tpl/footer.html")
	t.Execute(w, userGarden)
}

func GardenSave(w http.ResponseWriter, r *http.Request) {
	if !ValidateAccess(&w, r) {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	sess, err := store.Get(r, "session-name")
	sessUserID, _ := sess.Values["UserID"].(int)
	GardenName := r.FormValue("GardenName")

	if len(GardenName) <= 0 {
		http.Error(w, err.Error(), http.StatusBadRequest)
		log.Println("Missing 'GardenName' from request")
		return
	}

	result := dbCreateGarden(GardenName, sessUserID)

	t, _ := template.ParseFiles("tpl/GardenHome.html", "tpl/menu.html", "tpl/header.html", "tpl/footer.html")
	t.Execute(w, map[string]string{"Msg": result.Message})
}

func GardenCreate(w http.ResponseWriter, r *http.Request) {
	if !ValidateAccess(&w, r) {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	t, _ := template.ParseFiles("tpl/GardenCreate.html", "tpl/menu.html", "tpl/header.html", "tpl/footer.html")
	t.Execute(w, map[string]string{"Msg": "Garten erstellt"})
}
