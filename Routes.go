package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{"Index", "GET", "/", Index},
	Route{"createUser", "GET", "/register", RegisterForm},
	Route{"createUser", "POST", "/createUser", CreateUser},
	Route{"", "POST", "/login", LoginUser},
	Route{"", "GET", "/logout", LogoutUser},

	Route{"", "GET", "/addGarden", GardenCreate},
	Route{"", "POST", "/saveGarden", GardenSave},
	Route{"", "GET", "/myGarden/garden/{gardenID:[0-9]+}", GardenDetail},
	Route{"", "GET", "/myGarden", GardenHome},

	Route{"", "GET", "/encyclopedia", Encyclopedia},

	//	Route{"createGarden", "POST", "/garden/create", GardenCreate},
	//	Route{"showGarden", "GET", "/garden/show", GardenShow},
	//	Route{"showPlants", "GET", "/plants/", PlantsList},
}
