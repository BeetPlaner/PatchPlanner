$(document).ready(function(){


    if(($("#CreateUserInfo").html().length)<=0)
    {
        $("#CreateUserInfo").slideUp();
    }
    
    var hasError = false;


    $("#emailAddress").on('input', function() {
        isEmail()
    });

    $("#passwordConf").on('input', function() {
        passwordsOK()
    });


    $("#password").on('input', function() {
        passwordsOK()
    });

    function passwordsOK() {
        var pwdA = $("#passwordConf").val();
        var pwdB = $("#password").val();

        if(pwdA !=  pwdB) {
            $("#CreateUserInfo").html("Passwords not identical!").slideDown();
            return false;
        }
        else {
            $("#CreateUserInfo").slideUp();
            return true;
        }
    }

    $('#RegisterForm').submit(function(e) { 
        
        if(!isEmail() || !passwordsOK()) {
            alert("Please check mail and password!");
            e.preventDefault(); 
        }

   });

    function isEmail() {
        var email = $("#emailAddress").val();

        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        
        if(!regex.test(email))
        {
            $("#CreateUserInfo").html("Not a valid Emailaddress!").slideDown();
            return false;
        }
        else {
            $("#CreateUserInfo").slideUp();
            return true;
        }
      }
});