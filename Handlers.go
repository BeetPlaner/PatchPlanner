package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Serving index")

	if !ValidateAccess(&w, r) {
		t, _ := template.ParseFiles("tpl/login.html", "tpl/header.html", "tpl/footer.html")
		t.Execute(w, map[string]string{"Msg": ""})
	} else {
		t, _ := template.ParseFiles("tpl/dashboard.html", "tpl/menu.html", "tpl/header.html", "tpl/footer.html")
		t.Execute(w, map[string]string{"Msg": ""})
	}
}

func Encyclopedia(w http.ResponseWriter, r *http.Request) {
	if !ValidateAccess(&w, r) {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	t, _ := template.ParseFiles("tpl/dashboard.html", "tpl/menu.html", "tpl/header.html", "tpl/footer.html")
	t.Execute(w, map[string]string{"Msg": ""})
}

func ValidateAccess(w *http.ResponseWriter, r *http.Request) bool {
	// Get a session. Get() always returns a session, even if empty.
	session, err := store.Get(r, "session-name")
	if err != nil {
		http.Error(*w, err.Error(), http.StatusInternalServerError)
		log.Println("Error validating login")
		return false
	}

	log.Println(session)

	if sessionUsername, ok := session.Values["Username"].(string); ok {
		if sessionToken, ok := session.Values["LoginToken"].(string); ok {
			storedToken := dbGetLoginToken(string(sessionUsername))
			if storedToken == sessionToken && len(storedToken) > 1 {
				return true
			}
		}
	}

	log.Println("User not logged in!")
	return false
}

func RegisterForm(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("tpl/register.html", "tpl/header.html", "tpl/footer.html")
	t.Execute(w, map[string]string{"Msg": ""})
}

func LogoutUser(w http.ResponseWriter, r *http.Request) {

	// Get a session. Get() always returns a session, even if empty.
	session, err := store.Get(r, "session-name")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["LoginToken"] = ""
	session.Values["Username"] = ""
	session.Values["UserID"] = -1
	session.Save(r, w)

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func LoginUser(w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Get a session. Get() always returns a session, even if empty.
	session, err := store.Get(r, "session-name")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	Name := r.Form["emailAddress"][0]
	Password := r.Form["password"][0]

	if len(Name) == 0 || len(Password) == 0 {
		t, _ := template.ParseFiles("tpl/register.html", "tpl/header.html", "tpl/footer.html")
		rsp := GenericResult{
			Message: "Incomplete data!",
			Code:    -1,
		}

		t.Execute(w, rsp)
		return
	}

	t, ID := dbUserLogin(Name, Password)
	log.Println(t)

	session.Values["LoginToken"] = t.Message
	session.Values["Username"] = Name
	session.Values["UserID"] = ID
	session.Save(r, w)

	if t.Code < 0 {
		tmlpt, _ := template.ParseFiles("tpl/login.html", "tpl/header.html", "tpl/footer.html")
		tmlpt.Execute(w, t)
		return
	}

	http.Redirect(w, r, "/", http.StatusSeeOther)

}

func CreateUser(w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	Name := r.Form["emailAddress"][0]
	Password := r.Form["password"][0]

	if len(Name) == 0 || len(Password) == 0 {
		t, _ := template.ParseFiles("tpl/register.html", "tpl/header.html", "tpl/footer.html")
		rsp := GenericResult{
			Message: "Incomplete data!",
			Code:    -1,
		}

		t.Execute(w, rsp)
		return
	}

	t := dbUserCreate(Name, Password)

	if t.Code < 0 {
		log.Println(t)
		tmlpt, _ := template.ParseFiles("tpl/register.html", "tpl/header.html", "tpl/footer.html")
		tmlpt.Execute(w, t)
		return
	}

	http.Redirect(w, r, "/", http.StatusSeeOther)

}
