package main

import (
	"time"
)

/* Database-Models*/

type Garden struct {
	ID             int     `db:"ID"`
	Name           string  `db:"Name"`
	OwnerID        int     `db:"Owner"`
	RawCoordinates *string `db:"Coordinates"`
	Patches        []Patch
}

type Patch struct {
	ID       int    `db:"ID"`
	Name     string `db:"Name"`
	GardenID int    `db:"GardenID"`
}

type Planting struct {
	ID      int `db:"ID"`
	PlantID int `db:"PlantID"`
	PatchID int `db:"PatchID"`
}

type Event struct {
	ID   int       `db:"ID"`
	Desc string    `json:"desc"`
	Date time.Time `json:"date"`
	Ref  uint      `json:"ref"`
}

type Plant struct {
	Name             string `json:"name"`
	Description      string
	SeedFrom         time.Time
	SeedTo           time.Time
	HarvestFrom      time.Time
	HarvestTo        time.Time
	GrowDurationMin  time.Time
	GrowDurationMMax time.Time
	RowDistance      int
	ColDistance      int
}
