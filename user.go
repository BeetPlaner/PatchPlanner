package main

import "time"

type User struct {
	ID         int       `db:"ID"`
	Email      string    `db:"Email"`
	Password   string    `db:"Password"`
	Created    time.Time `db:"Created"`
	LastLogin  time.Time `db:"LastLogin"`
	IsVerified bool      `db:"UserVerified"`
	LoginToken string    `db:"LoginToken"`
}

type Users []User
